# README #

This is my rummage table for scripts that will accumulate over time. It's really a wild mixture, but maybe someone could benefit from them or use parts of them.

Please note that I am self tought in Python, so don't expect super pythonic nor "best practise" scripts... yet ;) But I'm trying to do an acceptable job for now and I'm always happy to get constructive criticism and to learn better ways to do things in python if you have suggestions.


## Files ##

#### clrmsg.py ####
Colored stdout status prefixes like [ DEBUG ], or [ ERROR ] etc. powered by "Colorama" (https://github.com/tartley/colorama)

#### csv_handler.py ####
Import and export PyQt model data from/to csv fles

#### scatterHistPlot.py ####
Draw a scatter plot with axis histograms (PyQT and Matplotlib)

#### stack_processing.py ####
Can be used as standalone application, i.e. run python -u interpolate_stack.py
or import interpolate_stack.py and use main function like:
interpolate_stack.main(imgpath, original_steppsize, interpolated_stepsize, interpolationmethod)

e.g: interpolate_stack("image_stack.tif", 300, 161.25, 'linear') => fast (~25x faster)
or: interpolate_stack("image_stack.tif", 300, 161.25, 'spline') => slow

where 300 is the focus step size the image stack was acquired with and 161.25 the step size
of the interpolated stack.

The spline method also returns a graph representing the interpolation in z of one x,y pixel in the
middle for comparison between the original data and the linear as well as the spline interpolation.

Options:
saveorigstack:		boolean	If an image sequence is used, in the form of "Tile_001-001-001_1-000.tif"
							(FEI MAPS/LA tif sequence naming scheme), this programm will save a single
							tiff stack file of the original images by default (True).
nointerpolation:	boolean	If set True (default is False) and saveorigstack is also True, no interpoaltion
							is done, only the packing of an image sequence to one single image stack file.
showgraph:			boolean	If set True (default False) and nointerpolation is False a graph is returned
							representing the interpolation in z of one x,y pixel in the middle of input stack
							for comparison between the original data and the linear as well as the spline interpolation.


